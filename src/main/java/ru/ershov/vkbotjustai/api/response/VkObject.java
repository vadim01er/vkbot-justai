package ru.ershov.vkbotjustai.api.response;

import lombok.Getter;

import java.io.Serializable;

@Getter
public class VkObject implements Serializable {
    private Message message;
}
