package ru.ershov.vkbotjustai.api.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.io.Serializable;

@Getter
public class Response implements Serializable {
    @JsonProperty("response")
    private long response;
}
