package ru.ershov.vkbotjustai.api.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.io.Serializable;

@Getter
public class Message implements Serializable {
    @JsonProperty("peer_id")
    private int peerId;
    private String text;
}
