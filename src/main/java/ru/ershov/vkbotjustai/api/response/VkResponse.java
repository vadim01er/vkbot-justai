package ru.ershov.vkbotjustai.api.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.io.Serializable;

@Getter
public class VkResponse implements Serializable {
    private String type;
    @JsonProperty("object")
    private VkObject vkObject;
}
