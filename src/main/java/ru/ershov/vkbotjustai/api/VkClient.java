package ru.ershov.vkbotjustai.api;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.ershov.vkbotjustai.api.response.Response;

import java.util.Random;

@Slf4j
@Service
@RequiredArgsConstructor
public class VkClient {

    private final RestTemplate restTemplate;

    @Value("${client.token}")
    private String token;
    @Value("${client.versionAPI}")
    private String versionApi;
    private final Random random = new Random();


    public void sendMessage(String message, int peerId) {
        String url = getUrlSendMessage(message, peerId);
        log.info("Trying GET request: " + url);
        ResponseEntity<Response> forEntity = restTemplate.getForEntity(url, Response.class);
        if (forEntity.hasBody() || forEntity.getStatusCode() == HttpStatus.OK) {
            log.info("Success");
        } else {
            log.error("Message not send");
        }
    }

    private String getUrlSendMessage(String message, int peerId) {
        return "https://api.vk.com/method/messages.send" + "?" +
                "peer_id=" + peerId + "&" +
                "access_token=" + token + "&" +
                "random_id=" + random.nextInt() + "&" +
                "v=" + versionApi + "&" +
                "message=" + message;
    }
}
