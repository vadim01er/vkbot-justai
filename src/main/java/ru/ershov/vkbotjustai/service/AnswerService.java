package ru.ershov.vkbotjustai.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.ershov.vkbotjustai.api.VkClient;
import ru.ershov.vkbotjustai.api.response.VkResponse;

@Service
@RequiredArgsConstructor
public class AnswerService {

    private static final String ANSWER_TEMPLATE = "Вы сказали: %s";

    private final VkClient vkClient;

    public void replay(VkResponse msg) {
        String textMsg = msg.getVkObject().getMessage().getText();
        int peerId = msg.getVkObject().getMessage().getPeerId();

        String answer = String.format(ANSWER_TEMPLATE, textMsg);
        vkClient.sendMessage(answer, peerId);
    }

}
