package ru.ershov.vkbotjustai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VkbotJustaiApplication {

    public static void main(String[] args) {
        SpringApplication.run(VkbotJustaiApplication.class, args);
    }

}
