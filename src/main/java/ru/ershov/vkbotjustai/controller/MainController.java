package ru.ershov.vkbotjustai.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.ershov.vkbotjustai.api.response.VkResponse;
import ru.ershov.vkbotjustai.service.AnswerService;

@RestController
@RequiredArgsConstructor
public class MainController {

    @Value("${client.confirmToken}")
    private String confirmToken;
    private final AnswerService answerService;

    @GetMapping
    public ResponseEntity<String> index() {
        return ResponseEntity.ok("If you want to use the bot's endpoint then: /botvk");
    }

    @GetMapping("/ping")
    public ResponseEntity<String> ping() {
        return ResponseEntity.ok("pong");
    }

    @PostMapping("/botvk")
    public String server(@RequestBody VkResponse msg) {
        if (msg.getType().equals("confirmation")) {
            return confirmToken;
        }

        answerService.replay(msg);

        return HttpStatus.OK.getReasonPhrase();
    }

}
